﻿using DomoviZdravlja.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DomoviZdravlja.GUI
{
    /// <summary>
    /// Interaction logic for FrmDodajIzmeni_Korisnik.xaml
    /// </summary>
    public partial class FrmDodajIzmeni_Korisnik : Window
    {
        Korisnik korisnikDI = null; //null ako se dodaje, postojeci objekat ako se pravi izmena
        bool regPacijent = false;//prosledjuje se true ako se registruje pacijent
        Korisnik korisnikLekarAdmin = null;//prilikom prijave lekara ili admina, za prikaz podataka o profilu
        //korisnikLekarAdmin kad se menja profil
        //upis lekara/admina:  null false null
        public FrmDodajIzmeni_Korisnik(Korisnik korisnikDI, bool regPacijent, Korisnik korisnikLekarAdmin)
        {
            InitializeComponent();
            this.korisnikDI = korisnikDI;
            this.regPacijent = regPacijent;
            this.korisnikLekarAdmin = korisnikLekarAdmin;

            cbTipKorisnika.Items.Add("lekar");
            cbTipKorisnika.Items.Add("pacijent");
            cbTipKorisnika.Items.Add("administrator");
            cbTipKorisnika.SelectedIndex = 0;

            cbDrzava.Items.Add("Srbija");
            cbDrzava.Items.Add("Bosna i Hercegovina");
            cbDrzava.Items.Add("Crna Gora");
            cbDrzava.SelectedIndex = 0;

            lbDomZdravlja.Visibility = Visibility.Visible;
            cbDomZdravlja.Visibility = Visibility.Visible;

            List<DomZdravlja> lstDomoviZdr = DbManagement.lstDomoviZdravlja.Where(x => x.getObrisan() == false).ToList();
            if (lstDomoviZdr.Count > 0)
            {
                foreach (DomZdravlja dz in lstDomoviZdr)
                    cbDomZdravlja.Items.Add(dz.Id + ") " + dz.Naziv);
                cbDomZdravlja.SelectedIndex = 0;
            }



            if (korisnikDI == null)//dodaje se nov korisnik
            {
                int noviAdresaId = DbManagement.ucitajAdrese().Max(x => x.Id) + 1;//generise id nove adrese
                tbIdAdresa.Text = noviAdresaId.ToString();
            }
            else//vrsi se izmena, setovati elemente forme prosledjenim objektom
            {

                tbJmbg.Text = korisnikDI.Jmbg;
                pbLozinka.Password = korisnikDI.getLozinka();
                tbImePrezime.Text = korisnikDI.ImePrezime;
                tbEmail.Text = korisnikDI.Email;
                if (korisnikDI.Pol == "muski")
                    rbMuski.IsChecked = true;
                else
                    rbZenski.IsChecked = true;
                cbTipKorisnika.Text = korisnikDI.TipKorisnika.ToString();
                if (korisnikDI.TipKorisnika.ToString() == "lekar")
                {
                    lbDomZdravlja.Visibility = Visibility.Visible;
                    cbDomZdravlja.Visibility = Visibility.Visible;
                    cbDomZdravlja.Text = korisnikDI.getDomZdravlja().Id + ") " + korisnikDI.getDomZdravlja().Naziv;
                }

                tbIdAdresa.Text = korisnikDI.getAdresa().Id.ToString();
                tbUlica.Text = korisnikDI.getAdresa().Ulica;
                tbBroj.Text = korisnikDI.getAdresa().Broj.ToString();
                tbGrad.Text = korisnikDI.getAdresa().Grad;
                cbDrzava.SelectedItem = korisnikDI.getAdresa().Drzava;
                btnDodaj.Content = "Izmeni";
                tbJmbg.IsEnabled = false;
            }


            if (regPacijent==true)//prosledjeno true
            {
                cbTipKorisnika.SelectedIndex = 1;
                cbTipKorisnika.IsEnabled = false;
                lbDomZdravlja.Visibility = Visibility.Hidden;
                cbDomZdravlja.Visibility = Visibility.Hidden;//sakriti domove zdravlja, ne m oze se reg kao lekar/admin

            }else if (korisnikLekarAdmin!=null && korisnikLekarAdmin.TipKorisnika == TipKorisnika.lekar)
            {
                cbTipKorisnika.SelectedIndex = 0;//lekar
                cbTipKorisnika.IsEnabled = false;//lekar se ne moze proglasiti adminom
            }
            else if (korisnikLekarAdmin != null && korisnikLekarAdmin.TipKorisnika == TipKorisnika.administrator)
            {
                cbTipKorisnika.SelectedIndex = 2;//admin
                cbTipKorisnika.IsEnabled = false;//ne moze menjati tip korisnika
                lbDomZdravlja.Visibility = Visibility.Hidden;//ne moze ni admin da bude lekar
                cbDomZdravlja.Visibility = Visibility.Hidden;
            }
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void cbTipKorisnika_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbTipKorisnika.SelectedItem.ToString() == "lekar")
            {
                lbDomZdravlja.Visibility = Visibility.Visible;
                cbDomZdravlja.Visibility = Visibility.Visible;
            }
            else
            {
                lbDomZdravlja.Visibility = Visibility.Hidden;
                cbDomZdravlja.Visibility = Visibility.Hidden;
            }
        }


        string validniPodaci()
        {
            string greske = "Greske:";
            if (tbJmbg.Text == "")
                greske += "\nMorate uneti jmbg!";
            if (pbLozinka.Password == "")
                greske += "\nMorate uneti lozinku!";
            if (tbImePrezime.Text == "")
                greske += "\nMorate uneti ime i prezime!";
            //if (tbEmail.Text == "")
                //greske += "\nMorate uneti email!";

            if (tbUlica.Text == "")
                greske += "\nMorate uneti naziv ulice!";
            if (tbBroj.Text == "")
                greske += "\nMorate uneti broj za adresu!";
            if (tbGrad.Text == "")
                greske += "\nMorate uneti naziv grada!";
            if (!int.TryParse(tbBroj.Text, out int broj))
                greske += "\nBroj adrese moraju biti cifre!";

            bool postojiJmbg = DbManagement.lstKorisnici.Any(k => k.Jmbg == tbJmbg.Text);
            if (postojiJmbg && this.korisnikDI==null)
                greske += "\nUneli ste postojeci jmbg!";

            return greske;

        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (validniPodaci() == "Greske:")//nema gresaka
            {

                int idAdresa = int.Parse(tbIdAdresa.Text);
                string ulica = tbUlica.Text;
                int broj = int.Parse(tbBroj.Text);
                string grad = tbGrad.Text;
                string drzava = cbDrzava.SelectedItem.ToString();
                bool obrisanAdresa = false;
                Adresa novaAdresa = new Adresa(idAdresa, ulica, broj, grad, drzava, obrisanAdresa);

                string jmbg = tbJmbg.Text;
                string lozinka = pbLozinka.Password.ToString();
                string imePrezime = tbImePrezime.Text;
                string email = tbEmail.Text;
                Adresa adresaObj = new Adresa(novaAdresa);
                string pol = "muski";
                if (rbZenski.IsChecked == true)
                    pol = "zenski";
                TipKorisnika tipKorisnika = (TipKorisnika)Enum.Parse(typeof(TipKorisnika), cbTipKorisnika.SelectedItem.ToString());
                DomZdravlja domZdravlja=null;
                if (tipKorisnika == TipKorisnika.lekar)
                {
                    int idDomZdr = int.Parse(cbDomZdravlja.SelectedItem.ToString().Split(')')[0]);
                    domZdravlja = DbManagement.lstDomoviZdravlja.Where(dz => dz.Id == idDomZdr).FirstOrDefault();
                }
                bool obrisan = false;

                //preuzeti elementi forme i pravi se objekat od njih
                Korisnik noviKorisnik = new Korisnik(jmbg, lozinka, imePrezime, email, adresaObj, pol, tipKorisnika, domZdravlja, obrisan);

                if (this.korisnikDI == null)//upis novog korisnika i njegove adrese
                {
                    DbManagement.lstKorisnici.Add(noviKorisnik);//dodaje se objekat u listu

                    DbManagement.insert_update_delete(  //poziva se metoda za upis u bazu
                        "insert into Adresa values(" +
                        idAdresa + ",'" +
                        ulica + "'," +
                        broj + ",'" +
                        grad + "','" +
                        drzava + "','" +
                        obrisanAdresa +
                        "');"
                        );


                    string insertIdDz = "', null";
                    if (domZdravlja != null)
                        insertIdDz = "', " + domZdravlja.Id;

                    DbManagement.insert_update_delete(
                        "insert into Korisnik values('" +
                        jmbg + "','" +
                        lozinka + "','" +
                        imePrezime + "','" +
                        email +"'," +
                        idAdresa + ",'" + 
                        pol + "','" + 
                        //tipKorisnika + "'," + 
                        //domZdravlja.Id + ",'" +
                        tipKorisnika + 
                        insertIdDz + ",'" +
                        obrisan +
                        "');"
                        );

                    MessageBox.Show("Uspesan upis korisnika.");
                }
                else//izmena
                {
                    for (int i = 0; i < DbManagement.lstKorisnici.Count; i++)
                    {
                        if (DbManagement.lstKorisnici.ElementAt(i).Jmbg == jmbg)
                        {
                            DbManagement.lstKorisnici.ElementAt(i).setLozinka(lozinka);
                            DbManagement.lstKorisnici.ElementAt(i).ImePrezime = imePrezime;
                            DbManagement.lstKorisnici.ElementAt(i).Email = email;

                            DbManagement.lstKorisnici.ElementAt(i).getAdresa().Ulica = ulica;
                            DbManagement.lstKorisnici.ElementAt(i).getAdresa().Broj = broj;
                            DbManagement.lstKorisnici.ElementAt(i).getAdresa().Grad = grad;
                            DbManagement.lstKorisnici.ElementAt(i).getAdresa().Drzava = drzava;
                            DbManagement.lstKorisnici.ElementAt(i).Adresa = DbManagement.lstKorisnici.ElementAt(i).getAdresa().toString();

                            DbManagement.lstKorisnici.ElementAt(i).Pol = pol;
                            DbManagement.lstKorisnici.ElementAt(i).TipKorisnika = tipKorisnika;
                            DbManagement.lstKorisnici.ElementAt(i).setDomZdravlja(domZdravlja);

                            break;
                        }
                    }

                    DbManagement.insert_update_delete(
                            "update Adresa set " +
                            "ulica='" + ulica +
                            "', broj=" + broj +
                            ", grad='" + grad +
                            "', drzava='" + drzava +
                            "' where Id=" + idAdresa +
                            ";"
                        );


                    string updateIdDz = "', IdDomZdravlja=null";
                    if (domZdravlja!=null)
                        updateIdDz = "', IdDomZdravlja=" + domZdravlja.Id;

                    DbManagement.insert_update_delete(
                           "update Korisnik set " +
                           "Lozinka='" + lozinka +
                           "', ImePrezime='" + imePrezime +
                           "', Email='" + email +
                           "', Pol='" + pol +
                           "', TipKorisnika='" + tipKorisnika +
                           updateIdDz +
                           " where Jmbg='" + jmbg +
                           "';"
                       );

                    MessageBox.Show("Uspesna izmena korisnika.");
                }


                DialogResult = true;//ovo se prati iz klase gde se pozvalo otvaranje prozora
            }
            else
            {
                MessageBox.Show(validniPodaci());
            }
        }
    }
}
