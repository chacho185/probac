﻿using DomoviZdravlja.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DomoviZdravlja.GUI
{
    /// <summary>
    /// Interaction logic for FrmDodavanjeTermina.xaml
    /// </summary>


    
public partial class FrmDodavanjeTermina : Window
    {
        Korisnik korisnikLekar = null;
        Korisnik korisnikPrijavljen = null;
        List<Terapija> lstTermini_LekarDatum=null;

        //korisnikLekar njegovi termini prikazani
        //korisnikPrijavljen za odredjivanje tipa korisnika, sta ce biti prikazano
        public FrmDodavanjeTermina(Korisnik korisnikLekar, Korisnik korisnikPrijavljen)
        {
            InitializeComponent();
            this.korisnikLekar = korisnikLekar;
            this.korisnikPrijavljen = korisnikPrijavljen;

           

            lbLekarTermini.Content = "Termini lekara: " + korisnikLekar.ImePrezime + ", Jmbg: " + korisnikLekar.Jmbg;//natpis na formi
            dtDatum.SelectedDate = DateTime.Now;


            for(int i=0; i < 24; i++)
                cbSati.Items.Add(i);
            for (int i = 0; i < 59; i++)
                cbMinuta.Items.Add(i); //inicijalizovano sati i minuta vreme
            cbSati.SelectedIndex = 9;
            cbMinuta.SelectedIndex = 0; //inicijalizovana polja za odabir sati minuta


            inicijalizujTermine_Datum();


            btnPrikaziProfil.Visibility = Visibility.Hidden;
            btnZakaziTermin.Visibility = Visibility.Hidden;
            if (korisnikPrijavljen != null)
            {
                if (korisnikPrijavljen.TipKorisnika == TipKorisnika.pacijent)
                {
                    sakrijOpcijeAdmina();
                    btnZakaziTermin.Visibility = Visibility.Visible;
                }
                if (korisnikPrijavljen.TipKorisnika == TipKorisnika.pacijent || korisnikPrijavljen.TipKorisnika == TipKorisnika.administrator)
                {
                    sakrijOpcijeLekara();
                }
                if (korisnikPrijavljen.TipKorisnika == TipKorisnika.lekar)
                    btnPrikaziProfil.Visibility = Visibility.Visible;
            }


        }
        void sakrijOpcijeAdmina()
        {
            lbVreme.Visibility = Visibility.Hidden;
            cbSati.Visibility = Visibility.Hidden;
            lbDveTackeSM.Visibility = Visibility.Hidden;
            cbMinuta.Visibility = Visibility.Hidden;
            btnDodajTermin.Visibility = Visibility.Hidden;
            btnObrisi.Visibility = Visibility.Hidden;
        }
        void sakrijOpcijeLekara()
        {
            btnDodajTerapiju.Visibility = Visibility.Hidden;
            btnPrikaziKarton.Visibility = Visibility.Hidden;
        }

        /*
        void inicijalizujTermine_Datum() //za svaku promenu date pickera inicijalizovati listom termina i terapija lekara 
        {
            lstTermini_LekarDatum = DbManagement.lstTerminiTerapije.Where(
                                                                      t => t.getLekar().Jmbg == korisnikLekar.Jmbg &&
                                                                      t.Datum.Date == dtDatum.SelectedDate.Value.Date &&
                                                                      t.getObrisan()==false
                                                                      ).ToList();
            lstTermini_LekarDatum = lstTermini_LekarDatum.OrderBy(t => t.Datum).ToList();

            lbTermini.Items.Clear();
            foreach(Terapija t in lstTermini_LekarDatum)
            {
                if (t.getPacijent() == null)
                {
                    lbTermini.Items.Add(t.Datum.Hour + ":" + t.Datum.Minute);

                    //if (lbTermini.Items.Count > 0)
                    //{
                    //    ListBoxItem lbi = (ListBoxItem)lbTermini.ItemContainerGenerator.ContainerFromIndex(0);
                    //    if(lbi!=null)
                    //    lbi.Background = Brushes.Green;
                    //}
                }
                else if (t.getPacijent() != null && t.OpisTerapije == "") //zakazani termini
                {
                    lbTermini.Items.Add(t.Datum.Hour + ":" + t.Datum.Minute + "# " + t.getPacijent().ImePrezime + "#" + t.getPacijent().Jmbg);
                }
                else if (t.getPacijent() != null && t.OpisTerapije != "")//zakazani termini s upisanom terapijom
                {
                    lbTermini.Items.Add(t.Datum.Hour + ":" + t.Datum.Minute + "# " + t.getPacijent().ImePrezime + "#" + t.getPacijent().Jmbg + "#" + t.OpisTerapije);
                }
            }
        }
        */
        
        
        void inicijalizujTermine_Datum() //za svaku promenu date pickera inicijalizovati listom termina i terapija lekara 
        {


            //MessageBox.Show(dtDatum.SelectedDate.Value.Date.ToShortDateString());
            //MessageBox.Show(DbManagement.lstTerminiTerapije.ElementAt(0).Datum.ToShortDateString());
            lstTermini_LekarDatum = DbManagement.lstTerminiTerapije.Where(
                                                                      t => t.getLekar().Jmbg == korisnikLekar.Jmbg &&
                                                                      t.Datum.Date.Month == dtDatum.SelectedDate.Value.Date.Month &&   //filtriranje za datum i lekara 
                                                                      t.Datum.Date.Date == dtDatum.SelectedDate.Value.Date.Date &&
                                                                      t.Datum.Date.Year == dtDatum.SelectedDate.Value.Date.Year &&
                                                                      t.getObrisan() == false
                                                                      ).ToList();
            lstTermini_LekarDatum = lstTermini_LekarDatum.OrderBy(t => t.Datum).ToList(); //rastuce po datumu
            //MessageBox.Show("lstTermini_LekarDatum.length:" + lstTermini_LekarDatum.Count);


            ObservableCollection<TerminPodaci> lstPodaciTermini = new ObservableCollection<TerminPodaci>();


            foreach (Terapija t in lstTermini_LekarDatum)
            {
                if (t.getPacijent() == null)
                {
                    lstPodaciTermini.Add(new TerminPodaci(t.Datum.Hour + ":" + t.Datum.Minute));//jos nije zakazan
                }
                else if (t.getPacijent() != null && t.OpisTerapije == "") //zakazani termini
                {
                    lstPodaciTermini.Add(new TerminPodaci(t.Datum.Hour + ":" + t.Datum.Minute + "# " + t.getPacijent().ImePrezime + "#" + t.getPacijent().Jmbg));
                }
                else if (t.getPacijent() != null && t.OpisTerapije != "")//zakazani termini s upisanom terapijom
                {
                    lstPodaciTermini.Add(new TerminPodaci(t.Datum.Hour + ":" + t.Datum.Minute + "# " + t.getPacijent().ImePrezime + "#" + t.getPacijent().Jmbg + "#" + t.OpisTerapije));
                }
            }

           
            dgTermini.ItemsSource = lstPodaciTermini;
            dgTermini.LoadingRow += dgTermini_LoadingRow;//event dodat na redove data grida da oboji zeleno, crveno ili narandzasto


        }

        private void dgTermini_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var row = e.Row;
            TerminPodaci TerminPod = row.DataContext as TerminPodaci;

            int count = TerminPod.Termin.Count(x => x == '#');
            if(count==0)
                row.Background = new SolidColorBrush(Colors.Green);//slobodni
            else if(count==2)
                row.Background = new SolidColorBrush(Colors.Red);//zakazani
            else if(count==3)
                row.Background = new SolidColorBrush(Colors.Orange);//s terapijom
        }


        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void dtDatum_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {

            //You can also use DateTime.Now.ToString("yyyy-MM-dd") for the date, and DateTime.Now.ToString("hh:mm:ss") for the time.

            inicijalizujTermine_Datum();
        }


        //dodaje u listu i u bazu
        void dodajTermin(string satiMinuta)
        {

            int id = 0;
            if(DbManagement.lstTerminiTerapije.Count>0)
                id=DbManagement.lstTerminiTerapije.Max(t=>t.Id);
            id++;//novi id 

            Korisnik lekarObj=new Korisnik(korisnikLekar);//svaki termin ima lekata
            Korisnik pacijentObj=null;//nema pacijenta dok ne zakaze, tad se setuje pacijent

            DateTime datum=dtDatum.SelectedDate.Value;
            int indexDveTacke = satiMinuta.IndexOf(':');
            string sati = satiMinuta.Substring(0, indexDveTacke);
            string minuta = satiMinuta.Substring(indexDveTacke + 1);
            TimeSpan ts = new TimeSpan(int.Parse(sati), int.Parse(minuta), 0);//treba selektovano vreme iz combo boxova staviti u datum
            datum = datum.Date + ts; //setuje odabrano vreme u odabrani datum
            //MessageBox.Show("datum:" + datum);

            bool statusDostupnost=true;
            bool obrisan=false;
            string opisTerapije="";//prazno, tek kad se zakaze i onda unese..
            //termin je zapravo terapija. Terapija nasledjuje Termin, dodato jos opis terapije
            Terapija terapija = new Terapija(id, lekarObj, pacijentObj, datum, statusDostupnost, obrisan, opisTerapije);
            DbManagement.lstTerminiTerapije.Add(terapija); //dodaje u listu

            DbManagement.insert_update_delete(
                    "insert into Termin values(" +
                     id + ",'" +
                     lekarObj.Jmbg + "'," +
                     "null,'" +
                     datum.ToString() + "','" +
                     statusDostupnost + "'," +
                     "null,'" +
                     obrisan +
                    "');"
                );

            inicijalizujTermine_Datum();//inicijalizuje listu i dodaje u listbox

        }
       

        private void btnDodajTermin_Click(object sender, RoutedEventArgs e)
        {

            string satiMinuta = formirajSatiMinuta();

            if (!postojiTermin(satiMinuta))
            {
                dodajTermin(satiMinuta);
               
            }
            else
                MessageBox.Show("Unet termin vec postoji!");
        }



        private void btnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (dgTermini.SelectedIndex > -1)
            {
                TerminPodaci tp = (TerminPodaci)dgTermini.SelectedItem;
                int count = tp.Termin.Count(x => x == '#');//ako je termin samo zakazan prikaz ne sadrzi znak #

                int indeksDveTacke = tp.Termin.IndexOf(":");
                string sati = tp.Termin.Substring(0, indeksDveTacke);
                string minuta = tp.Termin.Substring(indeksDveTacke + 1);
                if(count>0)
                    minuta = tp.Termin.Substring(indeksDveTacke + 1, minuta.IndexOf('#'));//sati i minuta na pocetku stringa
                //if (int.Parse(sati) < 10)
                //    sati = "0" + sati;
                //if (int.Parse(minuta) < 10)
                //    minuta = "0" + minuta;


                Terapija terminBrisanje = null;
                foreach(Terapija t in DbManagement.lstTerminiTerapije)
                {
                    if(t.Datum.Date.Date == dtDatum.SelectedDate.Value.Date.Date && t.Datum.Date.Month == dtDatum.SelectedDate.Value.Date.Month)
                    {
                        if (t.Datum.Hour.ToString().Equals(sati) && t.Datum.Minute.ToString().Equals(minuta))
                        {
                            terminBrisanje = t;//pronadjen na osnovu sati minuta
                        }
                    }
                }

                if (terminBrisanje != null)
                {
                    if (korisnikPrijavljen.TipKorisnika == TipKorisnika.lekar && count > 0)
                    {
                        MessageBox.Show("Lekar ne moze obrisati vec zakazan termin!");
                    }
                    else
                    {
                        //DbManagement.insert_update_delete("delete from Termin where id=" + terminBrisanje.Id + ";");
                        DbManagement.insert_update_delete("update Termin set obrisan='"+true+"' where id=" + terminBrisanje.Id + ";");
                        DbManagement.lstTerminiTerapije.Remove(terminBrisanje);
                        inicijalizujTermine_Datum();
                    }
                }

            }

        }



        string formirajSatiMinuta()
        {
            string sati = cbSati.SelectedItem.ToString();
            string minuta = cbMinuta.SelectedItem.ToString();


            //if (int.Parse(sati) < 10)
            //    sati = "0" + sati;
            //if (int.Parse(minuta) < 10)
            //    minuta = "0" + minuta;

            string satiMinuta = sati + ":" + minuta;
            return satiMinuta;
        }

        bool postojiTermin(string termin)
        {
            foreach (Termin t in lstTermini_LekarDatum)
            {
                string terminLista = t.Datum.Hour + ":" + t.Datum.Minute;
                if (termin.Equals(terminLista))
                    return true;
            }
                
            return false;
        }

        private void btnPrikaziProfil_Click(object sender, RoutedEventArgs e)
        {
            FrmDodajIzmeni_Korisnik frm = new FrmDodajIzmeni_Korisnik(korisnikPrijavljen, false, korisnikPrijavljen);
            frm.ShowDialog();
        }

        private void btnZakaziTermin_Click(object sender, RoutedEventArgs e)
        {
            if (dgTermini.SelectedIndex > -1 && korisnikPrijavljen.TipKorisnika==TipKorisnika.pacijent)
            {
                TerminPodaci tp = (TerminPodaci)dgTermini.SelectedItem;
                int count = tp.Termin.Count(x => x == '#');

                if (count == 0)//ako nema #, to znaci da je slobodan termin
                {
                    int indeksDveTacke = tp.Termin.IndexOf(":");
                    string sati = tp.Termin.Substring(0, indeksDveTacke);
                    string minuta = tp.Termin.Substring(indeksDveTacke + 1);


                    Terapija terapijaZakazi = null;
                    foreach (Terapija t in DbManagement.lstTerminiTerapije)
                    {
                        if (t.Datum.Date.Date == dtDatum.SelectedDate.Value.Date.Date && t.Datum.Date.Month == dtDatum.SelectedDate.Value.Date.Month)
                        {
                            if (t.Datum.Hour.ToString().Equals(sati) && t.Datum.Minute.ToString().Equals(minuta))
                            {
                                terapijaZakazi = t;
                                terapijaZakazi.setPacijent(korisnikPrijavljen); //terapiji dodat pacijent koji zakazuje termin
                            }
                        }
                    }

                    if (terapijaZakazi != null)
                    {
                        DbManagement.insert_update_delete("update Termin set JmbgPacijent='" + terapijaZakazi.getPacijent().Jmbg + "' where Id=" + terapijaZakazi.Id + ";");
                        for (int i = 0; i < DbManagement.lstTerminiTerapije.Count; i++)
                        {
                            if (DbManagement.lstTerminiTerapije.ElementAt(i).Id == terapijaZakazi.Id)
                            {
                                DbManagement.lstTerminiTerapije.ElementAt(i).setPacijent(terapijaZakazi.getPacijent());
                                break;
                            }
                        }
                        inicijalizujTermine_Datum();
                    }

                }
                else
                {
                    MessageBox.Show("Termin je vec zakazan !");
                }

            }
        }

        private void btnDodajTerapiju_Click(object sender, RoutedEventArgs e)
        {
            if (dgTermini.SelectedIndex > -1)
            {
                TerminPodaci tp = (TerminPodaci)dgTermini.SelectedItem;
                int count = tp.Termin.Count(x => x == '#');//ako je termin samo zakazan prikaz ne sadrzi znak #

                int indeksDveTacke = tp.Termin.IndexOf(":");
                string sati = tp.Termin.Substring(0, indeksDveTacke);
                string minuta = tp.Termin.Substring(indeksDveTacke + 1);
                if (count > 0)
                    minuta = tp.Termin.Substring(indeksDveTacke + 1, minuta.IndexOf('#'));//na osnovu sati minuta pronaci zakazan termin
                //if (int.Parse(sati) < 10)
                //    sati = "0" + sati;
                //if (int.Parse(minuta) < 10)
                //    minuta = "0" + minuta;


                Terapija terminUpisTerapije = null;
                foreach (Terapija t in DbManagement.lstTerminiTerapije)
                {
                    if (t.Datum.Date.Date == dtDatum.SelectedDate.Value.Date.Date && t.Datum.Date.Month == dtDatum.SelectedDate.Value.Date.Month)
                    {
                        if (t.Datum.Hour.ToString().Equals(sati) && t.Datum.Minute.ToString().Equals(minuta))
                        {
                            terminUpisTerapije = t;//pronadjen na osnovu sati minuta
                        }
                    }
                }



                if (terminUpisTerapije != null && terminUpisTerapije.getPacijent()!=null && korisnikPrijavljen.TipKorisnika==TipKorisnika.lekar)
                {

                    terminUpisTerapije.setLekar(korisnikPrijavljen);

                    FrmUpisTerapije frm = new FrmUpisTerapije(terminUpisTerapije);
                    frm.ShowDialog();

                    if (frm.DialogResult == true)
                        inicijalizujTermine_Datum();

                   
                }
                else
                {
                    MessageBox.Show("Termin nije zakazan!");
                }

            }
        }

        private void btnPrikaziKarton_Click(object sender, RoutedEventArgs e)
        {
            if (dgTermini.SelectedIndex > -1)
            {
                TerminPodaci tp = (TerminPodaci)dgTermini.SelectedItem;
                int count = tp.Termin.Count(x => x == '#');//ako je termin samo zakazan prikaz ne sadrzi znak #

                int indeksDveTacke = tp.Termin.IndexOf(":");
                string sati = tp.Termin.Substring(0, indeksDveTacke);
                string minuta = tp.Termin.Substring(indeksDveTacke + 1);
                if (count > 0)
                    minuta = tp.Termin.Substring(indeksDveTacke + 1, minuta.IndexOf('#'));
                //if (int.Parse(sati) < 10)
                //    sati = "0" + sati;
                //if (int.Parse(minuta) < 10)
                //    minuta = "0" + minuta;


                Terapija terminPrikazTerapija = null;
                foreach (Terapija t in DbManagement.lstTerminiTerapije)
                {
                    if (t.Datum.Date.Date == dtDatum.SelectedDate.Value.Date.Date && t.Datum.Date.Month == dtDatum.SelectedDate.Value.Date.Month)
                    {
                        if (t.Datum.Hour.ToString().Equals(sati) && t.Datum.Minute.ToString().Equals(minuta))
                        {
                            terminPrikazTerapija = t;
                        }
                    }
                }

                if (terminPrikazTerapija != null && terminPrikazTerapija.OpisTerapije!="" && (korisnikPrijavljen.TipKorisnika==TipKorisnika.administrator || korisnikPrijavljen.TipKorisnika==TipKorisnika.lekar))
                {

                    FrmPacijent frm = new FrmPacijent(terminPrikazTerapija.getPacijent(), korisnikPrijavljen);
                    frm.ShowDialog();


                }
                else
                {
                    MessageBox.Show("Termin nije zakazan!");
                }

            }
        }

        //bool postojiTermin(string termin)
        //{
        //    foreach (string el in lbTermini.Items)
        //        if (el == termin)
        //            return true;
        //    return false;
        //}
    }
}
