﻿using DomoviZdravlja.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DomoviZdravlja.GUI
{
    /// <summary>
    /// Interaction logic for FrmPrijava.xaml
    /// </summary>
    public partial class FrmPrijava : Window
    {
        Button btnPrijava = null;
        Label lbPrijavljenKorisnik = null;
        Button btnPrikazProfila = null;
        Button btnRegistracijaPacijenta = null;
        Button btnZakaziTermin = null;

        public FrmPrijava(Button btnPrijava, Label lbPrijavljenKorisnik, Button btnPrikazProfila, Button btnRegistracijaPacijenta, Button btnZakaziTermin)
        {
            InitializeComponent();

            cbTipKorisnika.Items.Add("Lekar");
            cbTipKorisnika.Items.Add("Pacijent");
            cbTipKorisnika.Items.Add("Administrator");
            cbTipKorisnika.SelectedIndex = 0;

            this.btnPrijava = btnPrijava;
            this.lbPrijavljenKorisnik = lbPrijavljenKorisnik;
            this.btnPrikazProfila = btnPrikazProfila;
            this.btnRegistracijaPacijenta = btnRegistracijaPacijenta;
            this.btnZakaziTermin = btnZakaziTermin;//samo prijavljeni pacijent
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnPrijaviSe_Click(object sender, RoutedEventArgs e)
        {
            string korisnickoIme = tbKorisnickoIme.Text;
            string lozinka = pbLozinka.Password;

            if(korisnickoIme=="" || lozinka == "")
            {
                MessageBox.Show("Morate uneti korisnicko ime i lozinku!");
            }
            else
            {
                bool postoji = false;
                foreach(Korisnik k in DbManagement.lstKorisnici)
                {
                    if(k.Jmbg.Equals(korisnickoIme) && k.getLozinka().Equals(lozinka) && k.TipKorisnika.ToString().Equals(cbTipKorisnika.SelectedItem.ToString().ToLower()))
                    {
                        postoji = true;
                        this.Close();
                        this.btnPrijava.Content = "Odjava";
                        this.lbPrijavljenKorisnik.Content = "Prijavljen korisnik: " + k.ImePrezime + ", jmbg: " + k.Jmbg + ", Tip korisnika: " + k.TipKorisnika.ToString();
                        this.lbPrijavljenKorisnik.Visibility = Visibility.Visible;
                        this.btnPrikazProfila.Visibility = Visibility.Visible;
                        this.btnRegistracijaPacijenta.Visibility = Visibility.Hidden;
                        MainWindow.prijavljenKorisnik = new Korisnik(k);


                        if (k.TipKorisnika.ToString().Equals("lekar"))
                        {

                            //FrmLekar frm = new FrmLekar(k);
                            //frm.ShowDialog();
                            FrmDodavanjeTermina frm = new FrmDodavanjeTermina(k, k);
                            frm.ShowDialog();
                            break;
                        }
                        else if (k.TipKorisnika.ToString().Equals("pacijent"))
                        {
                            this.btnZakaziTermin.Visibility = Visibility.Visible;
                            FrmPacijent frm = new FrmPacijent(k, k);
                            frm.ShowDialog();
                            break;

                        }
                        else if (k.TipKorisnika.ToString().Equals("administrator"))
                        {
                            FrmAdmin frm = new FrmAdmin(k);
                            frm.ShowDialog();
                            break;
                        }

                        DialogResult = true;

                    }

                   
                }
                if (!postoji)
                    MessageBox.Show("Uneli ste pogresno korisnicko ime i/ili lozinku ili ste odabrali pogresan tip korisnika !");
            }

        }
    }
}
