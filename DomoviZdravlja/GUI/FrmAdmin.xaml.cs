﻿using DomoviZdravlja.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DomoviZdravlja.GUI
{
    /// <summary>
    /// Interaction logic for FrmAdmin.xaml
    /// </summary>
    public partial class FrmAdmin : Window
    {
        Korisnik korisnikPrijavljenAdmin = null;
        public FrmAdmin(Korisnik korisnikPrijavljenAdmin)
        {
            InitializeComponent();
            this.korisnikPrijavljenAdmin = korisnikPrijavljenAdmin;
            dg1.ItemsSource = DbManagement.lstKorisnici.Where(x=>x.getObrisan()==false);

            cbTipKorisnika.Items.Add("Svi");
            cbTipKorisnika.Items.Add("Lekari");
            cbTipKorisnika.Items.Add("Pacijenti");
            cbTipKorisnika.Items.Add("Administratori");
            cbTipKorisnika.SelectedIndex = 0;
        }

        private void rbKorisnici_Checked(object sender, RoutedEventArgs e)
        {
            if(btnTermini!=null)
                 btnTermini.Visibility = Visibility.Visible;

            if (lbTipKorisnika != null && cbTipKorisnika != null)
            {
                lbTipKorisnika.Visibility = Visibility.Visible;
                cbTipKorisnika.Visibility = Visibility.Visible;
                cbTipKorisnika.SelectedIndex = 0;
            }

            if (dg1!=null)
                 dg1.ItemsSource= DbManagement.lstKorisnici.Where(x => x.getObrisan() == false);
        }

        private void rbDomoviZdravlja_Checked(object sender, RoutedEventArgs e)
        {
            btnTermini.Visibility = Visibility.Hidden;
            lbTipKorisnika.Visibility = Visibility.Hidden;
            cbTipKorisnika.Visibility = Visibility.Hidden;

            if (dg1!=null)
             dg1.ItemsSource = DbManagement.lstDomoviZdravlja.Where(x => x.getObrisan() == false);

        }

        private void btnBrisi_Click(object sender, RoutedEventArgs e)
        {
            if (dg1.SelectedIndex > -1)
            {
                if (rbKorisnici.IsChecked == true)
                {
                    FrmPotvrdaBrisanja frm = new FrmPotvrdaBrisanja();
                    frm.ShowDialog();

                    if (frm.DialogResult == true)
                    {
                        Korisnik k = (Korisnik)dg1.SelectedItem;
                        for (int i = 0; i < DbManagement.lstKorisnici.Count; i++)
                        {
                            if (DbManagement.lstKorisnici.ElementAt(i).Jmbg.Equals(k.Jmbg))
                            {
                                DbManagement.lstKorisnici.ElementAt(i).obrisi();
                                DbManagement.insert_update_delete("update korisnik set obrisan='True' where jmbg=" + k.Jmbg);
                                //dg1.ItemsSource = DbManagement.lstKorisnici.Where(x => x.getObrisan() == false);
                                osveziKorisniciPrikaz();
                                break;
                            }
                        }
                    }
                }
                if (rbDomoviZdravlja.IsChecked == true)
                {
                    FrmPotvrdaBrisanja frm = new FrmPotvrdaBrisanja();
                    frm.ShowDialog();

                    if (frm.DialogResult == true)//na otvorenoj formi kliknuto potvrdno
                    {
                        DomZdravlja d = (DomZdravlja)dg1.SelectedItem;//preuzet objekat iz dg
                        for (int i = 0; i < DbManagement.lstDomoviZdravlja.Count; i++)
                        {
                            if (DbManagement.lstDomoviZdravlja.ElementAt(i).Id.Equals(d.Id))//pronadjen taj objekat u listi dz
                            {
                                DbManagement.lstDomoviZdravlja.ElementAt(i).obrisi();//setuje Obrisan na true
                                DbManagement.insert_update_delete("update domzdravlja set obrisan='True' where id=" + d.Id);
                                dg1.ItemsSource = DbManagement.lstDomoviZdravlja.Where(x => x.getObrisan() == false);//opet isfiltrira koji su obrisan==false
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (rbDomoviZdravlja.IsChecked==true)
            {
                FrmDodajIzmeni_DomZdravlja frm = new FrmDodajIzmeni_DomZdravlja(null);
                frm.ShowDialog();
                if(frm.DialogResult==true)//ako je na otvorenoj formi kliknuto dodavanje da osvezi prikaz data grida
                    dg1.ItemsSource = DbManagement.lstDomoviZdravlja.Where(x => x.getObrisan() == false);
            }
            if (rbKorisnici.IsChecked == true)
            {
                FrmDodajIzmeni_Korisnik frm = new FrmDodajIzmeni_Korisnik(null, false, null);
                frm.ShowDialog();
                if (frm.DialogResult == true)
                    osveziKorisniciPrikaz();

            }
        }

        private void btnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (rbDomoviZdravlja.IsChecked == true && dg1.SelectedIndex>-1)//ako su odabrani domovi zdr i ako je selektovan dz iz data grida
            {
                DomZdravlja domZdrSel = (DomZdravlja)dg1.SelectedItem;//napraljen objekat od selektovanog reda iz dg
                FrmDodajIzmeni_DomZdravlja frm = new FrmDodajIzmeni_DomZdravlja(domZdrSel);//formi poslat objekat na izmenu
                frm.ShowDialog();
                if (frm.DialogResult == true)//ako se potvrdilo, kliknuto na dodaj, refresh data grida
                    dg1.ItemsSource = DbManagement.lstDomoviZdravlja.Where(x => x.getObrisan() == false);
            }
            else if (rbKorisnici.IsChecked == true && dg1.SelectedIndex > -1)
            {
                Korisnik korisnikSel = (Korisnik)dg1.SelectedItem;
                FrmDodajIzmeni_Korisnik frm = new FrmDodajIzmeni_Korisnik(korisnikSel, false, null);
                frm.ShowDialog();
                if (frm.DialogResult == true)
                    osveziKorisniciPrikaz();
            }
        }

        private void cbTipKorisnika_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            osveziKorisniciPrikaz();
        }

        void osveziKorisniciPrikaz() {

            if (cbTipKorisnika.SelectedIndex == 0)
                dg1.ItemsSource = DbManagement.lstKorisnici.Where(x => x.getObrisan() == false);
            if (cbTipKorisnika.SelectedIndex == 1)
                dg1.ItemsSource = DbManagement.lstKorisnici.Where(x => x.getObrisan() == false && x.TipKorisnika == TipKorisnika.lekar);
            if (cbTipKorisnika.SelectedIndex == 2)
                dg1.ItemsSource = DbManagement.lstKorisnici.Where(x => x.getObrisan() == false && x.TipKorisnika == TipKorisnika.pacijent);
            if (cbTipKorisnika.SelectedIndex == 3)
                dg1.ItemsSource = DbManagement.lstKorisnici.Where(x => x.getObrisan() == false && x.TipKorisnika == TipKorisnika.administrator);
        }

        private void btnTermini_Click(object sender, RoutedEventArgs e)
        {
            if (dg1.SelectedIndex > -1)
            {
                Korisnik k = (Korisnik)dg1.SelectedItem;
                if (k.TipKorisnika == TipKorisnika.lekar)
                {
                    FrmDodavanjeTermina frm = new FrmDodavanjeTermina(k, korisnikPrijavljenAdmin);
                    frm.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Morate selektovati lekara!");
                }
            }
            else
            {
                MessageBox.Show("Morate selektovati lekara!");
            }
        }

        private void btnPrikaziProfil_Click(object sender, RoutedEventArgs e)
        {
            FrmDodajIzmeni_Korisnik frm = new FrmDodajIzmeni_Korisnik(korisnikPrijavljenAdmin, false, korisnikPrijavljenAdmin);
            frm.ShowDialog();
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void tbPretraga_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (rbDomoviZdravlja.IsChecked == true)
            {
                dg1.ItemsSource = DbManagement.lstDomoviZdravlja.Where(
                    dz =>
                    dz.getObrisan()==false && (
                    dz.Naziv.ToLower().Contains(tbPretraga.Text.ToLower()) ||
                    dz.getAdresa().Ulica.ToLower().Contains(tbPretraga.Text.ToLower()) ||
                    dz.getAdresa().Broj.ToString().Contains(tbPretraga.Text)
                    )
                    );
            }
            else if (rbKorisnici.IsChecked == true)
            {
                dg1.ItemsSource = DbManagement.lstKorisnici.Where(
                       k =>
                       k.getObrisan()==false && (
                       k.ImePrezime.ToLower().Contains(tbPretraga.Text.ToLower()) ||
                       k.Email.ToLower().Contains(tbPretraga.Text.ToLower()) ||
                       k.getAdresa().Ulica.ToLower().Contains(tbPretraga.Text.ToLower()) ||
                       k.getAdresa().Broj.ToString().Contains(tbPretraga.Text)
                       )
                    );
            }
        }
    }
}
