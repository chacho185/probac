﻿using DomoviZdravlja.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DomoviZdravlja.GUI
{
    /// <summary>
    /// Interaction logic for FrmPacijent.xaml
    /// </summary>
    public partial class FrmPacijent : Window
    {
        Korisnik korisnikPacijent = null;
        Korisnik korisnikPrijavljen = null;
        public FrmPacijent(Korisnik korisnik, Korisnik korisnikPrijavljen)
        {
            InitializeComponent();
            this.korisnikPacijent = korisnik;
            this.korisnikPrijavljen = korisnikPrijavljen;

            lbTerapije.Content = "Terapije pacijenta: " + korisnik.ImePrezime + ", Jmbg: " + korisnik.Jmbg;


            if (korisnikPrijavljen.TipKorisnika == TipKorisnika.pacijent)
            {
                btnPrikaziProfil.Visibility = Visibility.Visible;
            }
            else
            {
                btnPrikaziProfil.Visibility = Visibility.Hidden;
                this.Title = "Terapije";
            }

            List<Terapija> lstTerapije = new List<Terapija>();
            foreach(Terapija t in DbManagement.lstTerminiTerapije)
            {
                if(t.getPacijent()!=null)
                    if (t.getPacijent().Jmbg == korisnikPacijent.Jmbg && t.OpisTerapije != "")
                        lstTerapije.Add(t);
            }
            if (lstTerapije != null)
                dgTerapije.ItemsSource = lstTerapije;

        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnPrikaziProfil_Click(object sender, RoutedEventArgs e)
        {
            FrmDodajIzmeni_Korisnik frm = new FrmDodajIzmeni_Korisnik(korisnikPacijent, true, null);
            frm.ShowDialog();

        }
    }
}
