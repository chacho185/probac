﻿using DomoviZdravlja.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DomoviZdravlja.GUI
{
    /// <summary>
    /// Interaction logic for FrmLekar.xaml
    /// </summary>
    public partial class FrmLekar : Window
    {
        Korisnik korisnik = null;
        public FrmLekar(Korisnik korisnik)
        {
            InitializeComponent();
            this.korisnik = korisnik;
        }

        private void btnPrikaziProfil_Click(object sender, RoutedEventArgs e)
        {
            FrmDodajIzmeni_Korisnik frm = new FrmDodajIzmeni_Korisnik(korisnik, false, korisnik);
            frm.ShowDialog();
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
