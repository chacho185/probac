﻿using DomoviZdravlja.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DomoviZdravlja.GUI
{
    /// <summary>
    /// Interaction logic for FrmUpisTerapije.xaml
    /// </summary>
    public partial class FrmUpisTerapije : Window
    {
        Terapija terapija = null;
        public FrmUpisTerapije(Terapija terapija)
        {
            InitializeComponent();
            this.terapija = terapija;
            lbUpisTerapije.Content = "Termin: " + terapija.Datum + ", Pacijent: " + terapija.getPacijent().ImePrezime + ", Jmbg: " + terapija.getPacijent().Jmbg;
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (tbOpis.Text == "")
            {
                MessageBox.Show("Morate uneti opis!");
            }
            else
            {
                for(int i=0; i< DbManagement.lstTerminiTerapije.Count; i++)
                {
                    if (DbManagement.lstTerminiTerapije.ElementAt(i).Id == terapija.Id)
                    {
                        DbManagement.lstTerminiTerapije.ElementAt(i).OpisTerapije = tbOpis.Text;
                        DbManagement.lstTerminiTerapije.ElementAt(i).setLekar(terapija.getLekar());
                        DbManagement.lstTerminiTerapije.ElementAt(i).setPacijent(terapija.getPacijent());
                        DbManagement.lstTerminiTerapije.ElementAt(i).Lekar =
                            terapija.getLekar().Jmbg + ", Ime i prezime: " + terapija.getLekar().ImePrezime;
                        DbManagement.lstTerminiTerapije.ElementAt(i).Pacijent =
                            terapija.getPacijent().Jmbg + ", Ime i prezime: " + terapija.getPacijent().ImePrezime;
                        break;
                    }
                }

                DbManagement.insert_update_delete("update Termin set OpisTerapije='" + tbOpis.Text + "', JmbgLekar='"+terapija.getLekar().Jmbg+ "', JmbgPacijent='"+terapija.getPacijent().Jmbg+"' where Id=" + terapija.Id + ";");
                DialogResult = true;
            }

        }
    }
}
