﻿using DomoviZdravlja.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DomoviZdravlja.GUI
{
    /// <summary>
    /// Interaction logic for FrmDodajIzmeni_DomZdravlja.xaml
    /// </summary>
    public partial class FrmDodajIzmeni_DomZdravlja : Window
    {
        DomZdravlja domZdravlja_DI = null;//null za dodavanje, !=null za izmenu podataka 
        public FrmDodajIzmeni_DomZdravlja(DomZdravlja domZdravlja_DI)
        {
            InitializeComponent();
            this.domZdravlja_DI = domZdravlja_DI;
            tbIdDomZdravlja.IsEnabled = false;
            tbIdAdresa.IsEnabled = false;

            cbDrzava.Items.Add("Srbija");
            cbDrzava.Items.Add("Bosna i Hercegovina");
            cbDrzava.Items.Add("Crna Gora");
            cbDrzava.SelectedIndex = 0;

            if (domZdravlja_DI == null)
            {
                int noviDzId = DbManagement.lstDomoviZdravlja.Max(x => x.Id) + 1;
                int noviAdresaId = DbManagement.ucitajAdrese().Max(x => x.Id) + 1;
                tbIdDomZdravlja.Text = noviDzId.ToString();
                tbIdAdresa.Text = noviAdresaId.ToString();

            }
            else {
                //izmena podataka, elementima forme dodeliti elemente prosledjenog objekta, objekat razlit od null
                tbIdDomZdravlja.Text = domZdravlja_DI.Id.ToString();
                tbNaziv.Text = domZdravlja_DI.Naziv;

                tbIdAdresa.Text = domZdravlja_DI.getAdresa().Id.ToString();
                tbUlica.Text = domZdravlja_DI.getAdresa().Ulica;
                tbBroj.Text = domZdravlja_DI.getAdresa().Broj.ToString();
                tbGrad.Text = domZdravlja_DI.getAdresa().Grad;
                cbDrzava.SelectedItem = domZdravlja_DI.getAdresa().Drzava;
                btnDodaj.Content = "Izmeni";

            }


        }

        string validniPodaci()
        {
            string greske = "Greske:";
            if (tbNaziv.Text == "")
                greske += "\nMorate uneti naziv doma zdravlja!";
            if (tbUlica.Text == "")
                greske += "\nMorate uneti naziv ulice!";
            if (tbBroj.Text == "")
                greske += "\nMorate uneti broj za adresu!";
            if (tbGrad.Text == "")
                greske += "\nMorate uneti naziv grada!";
            if (!int.TryParse(tbBroj.Text, out int broj))
                greske += "\nBroj adrese moraju biti cifre!";

            return greske;

        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (validniPodaci() == "Greske:")//u okviru metode na string Greske: nista nije dodato, nema gresaka
            {

                int idAdresa=int.Parse(tbIdAdresa.Text);
                string ulica=tbUlica.Text;
                int broj=int.Parse(tbBroj.Text);
                string grad=tbGrad.Text;
                string drzava=cbDrzava.SelectedItem.ToString();
                bool obrisanAdresa = false;
                Adresa novaAdresa = new Adresa(idAdresa, ulica, broj, grad, drzava, obrisanAdresa);

                int idDz=int.Parse(tbIdDomZdravlja.Text);
                string naziv=tbNaziv.Text;
                Adresa adresaObj=new Adresa(novaAdresa);
                bool obrisanDz = false;
                DomZdravlja noviDomZdravlja = new DomZdravlja(idDz, naziv, adresaObj, obrisanDz);

                if (this.domZdravlja_DI == null)//upis novog DZ i njegove adrese
                {
                    DbManagement.lstDomoviZdravlja.Add(noviDomZdravlja);

                    DbManagement.insert_update_delete(
                        "insert into Adresa values(" +
                        idAdresa + ",'" +
                        ulica + "'," + 
                        broj + ",'"+
                        grad + "','"+
                        drzava+"','"+
                        obrisanAdresa+
                        "');"
                        );

                    DbManagement.insert_update_delete(
                        "insert into DomZdravlja values(" +
                        idDz + ",'" +
                        naziv + "',"+
                        adresaObj.Id + ",'"+
                        obrisanDz +
                        "');"
                        );
                }
                else
                {
                    for(int i=0; i<DbManagement.lstDomoviZdravlja.Count; i++)
                    {
                        if (DbManagement.lstDomoviZdravlja.ElementAt(i).Id == idDz)//pronadjen dz na osnovu id doma zdravlja
                        {
                            DbManagement.lstDomoviZdravlja.ElementAt(i).Naziv = naziv;
                            DbManagement.lstDomoviZdravlja.ElementAt(i).getAdresa().Ulica = ulica;
                            DbManagement.lstDomoviZdravlja.ElementAt(i).getAdresa().Broj = broj;
                            DbManagement.lstDomoviZdravlja.ElementAt(i).getAdresa().Grad = grad;
                            DbManagement.lstDomoviZdravlja.ElementAt(i).getAdresa().Drzava = drzava;
                            DbManagement.lstDomoviZdravlja.ElementAt(i).Adresa = DbManagement.lstDomoviZdravlja.ElementAt(i).getAdresa().toString();

                            break;
                        }
                    }

                    DbManagement.insert_update_delete(
                            "update Adresa set "+
                            "ulica='"+ulica +
                            "', broj="+broj +
                            ", grad='"+grad +
                            "', drzava='" + drzava + 
                            "' where Id="+idAdresa +
                            ";"
                        );
                    DbManagement.insert_update_delete(
                           "update DomZdravlja set " +
                           "naziv='" + naziv +
                           "' where Id=" + idDz +
                           ";"
                       );
                }





                DialogResult = true;
            }
            else
            {
                MessageBox.Show(validniPodaci());
            }
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
