﻿#pragma checksum "..\..\..\GUI\FrmDodavanjeTermina.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "7F4EF0ACEA620F36BF734DB2D81DF35D20A91CC649AC4961C9AF7BC09F72D8DB"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DomoviZdravlja.GUI;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DomoviZdravlja.GUI {
    
    
    /// <summary>
    /// FrmDodavanjeTermina
    /// </summary>
    public partial class FrmDodavanjeTermina : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbLekarTermini;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dtDatum;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnZatvori;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbVreme;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbSati;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbMinuta;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbDveTackeSM;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDodajTermin;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbTerminiNatpis;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnObrisi;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDodajTerapiju;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPrikaziKarton;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgTermini;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPrikaziProfil;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnZakaziTermin;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DomoviZdravlja;component/gui/frmdodavanjetermina.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lbLekarTermini = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.dtDatum = ((System.Windows.Controls.DatePicker)(target));
            
            #line 12 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
            this.dtDatum.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dtDatum_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnZatvori = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
            this.btnZatvori.Click += new System.Windows.RoutedEventHandler(this.btnZatvori_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.lbVreme = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.cbSati = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.cbMinuta = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.lbDveTackeSM = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.btnDodajTermin = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
            this.btnDodajTermin.Click += new System.Windows.RoutedEventHandler(this.btnDodajTermin_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.lbTerminiNatpis = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.btnObrisi = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
            this.btnObrisi.Click += new System.Windows.RoutedEventHandler(this.btnObrisi_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btnDodajTerapiju = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
            this.btnDodajTerapiju.Click += new System.Windows.RoutedEventHandler(this.btnDodajTerapiju_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btnPrikaziKarton = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
            this.btnPrikaziKarton.Click += new System.Windows.RoutedEventHandler(this.btnPrikaziKarton_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.dgTermini = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 14:
            this.btnPrikaziProfil = ((System.Windows.Controls.Button)(target));
            
            #line 24 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
            this.btnPrikaziProfil.Click += new System.Windows.RoutedEventHandler(this.btnPrikaziProfil_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btnZakaziTermin = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\..\GUI\FrmDodavanjeTermina.xaml"
            this.btnZakaziTermin.Click += new System.Windows.RoutedEventHandler(this.btnZakaziTermin_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

