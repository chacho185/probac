﻿using DomoviZdravlja.GUI;
using DomoviZdravlja.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DomoviZdravlja
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public static Korisnik prijavljenKorisnik = null;
        public MainWindow()
        {
            InitializeComponent();
            DbManagement.ucitajListe();//pokretanjem se ucitaju svi podaci u staticne liste u DbManagement klasi

            //string terapijeDat = "#";
            //foreach (Terapija t in DbManagement.lstTerminiTerapije)
            //    terapijeDat += t.Datum+ "#";
            //MessageBox.Show(terapijeDat);



            dg1.ItemsSource = DbManagement.lstKorisnici.Where(x=>x.getObrisan()==false);
            lbPrijavljenKorisnik.Visibility = Visibility.Hidden;
            btnPrikazProfila.Visibility = Visibility.Hidden;
            btnZakaziTermin.Visibility = Visibility.Hidden;


            cbTipKorisnika.Items.Add("Lekari i pacijenti");
            cbTipKorisnika.Items.Add("Lekari");
            cbTipKorisnika.Items.Add("Pacijenti");
            cbTipKorisnika.SelectedIndex = 0;



        }

        private void rbKorisnici_Checked(object sender, RoutedEventArgs e)
        {
            if (lbTipKorisnika != null && cbTipKorisnika != null)
            {
                lbTipKorisnika.Visibility = Visibility.Visible;
                cbTipKorisnika.Visibility = Visibility.Visible;
            }
            dg1.ItemsSource = DbManagement.lstKorisnici.Where(x => x.getObrisan() == false);
        }

        private void rbDomoviZdravlja_Checked(object sender, RoutedEventArgs e)
        {
            lbTipKorisnika.Visibility = Visibility.Hidden;
            cbTipKorisnika.Visibility = Visibility.Hidden;
            dg1.ItemsSource = DbManagement.lstDomoviZdravlja.Where(x => x.getObrisan() == false);
        }

        private void rbTerapije_Checked(object sender, RoutedEventArgs e)
        {
            lbTipKorisnika.Visibility = Visibility.Hidden;
            cbTipKorisnika.Visibility = Visibility.Hidden;
            ObservableCollection<Terapija> lstTerapije = new ObservableCollection<Terapija>();
            foreach(Termin t in DbManagement.lstTerminiTerapije)
            {
                if(t is Terapija)
                {
                    lstTerapije.Add((Terapija)t);
                }
            }

            dg1.ItemsSource = lstTerapije.Where(x => x.getObrisan() == false);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (btnPrijava.Content.Equals("Odjava"))
            {
                btnPrijava.Content = "Prijava korisnika";  //kad se odjavi opet pise prijava korisnika
                prijavljenKorisnik = null;//nijedan nije prijavljen
                lbPrijavljenKorisnik.Visibility = Visibility.Hidden;
                btnPrikazProfila.Visibility = Visibility.Hidden;//nije prijavljen
                btnRegistracijaPacijenta.Visibility = Visibility.Visible;
                btnZakaziTermin.Visibility = Visibility.Hidden;
            }
            else
            {

                FrmPrijava frm = new FrmPrijava(btnPrijava, lbPrijavljenKorisnik, btnPrikazProfila, btnRegistracijaPacijenta, btnZakaziTermin);
                frm.ShowDialog();

               
               

            }
        }

        private void btnPrikazProfila_Click(object sender, RoutedEventArgs e)
        {
            if (prijavljenKorisnik != null)
            {
                if (prijavljenKorisnik.TipKorisnika.ToString() == "lekar")
                {
                    //FrmLekar frm = new FrmLekar(prijavljenKorisnik);
                    //frm.ShowDialog();

                    FrmDodavanjeTermina frm = new FrmDodavanjeTermina(prijavljenKorisnik, prijavljenKorisnik);
                    frm.ShowDialog();

                }
                else if (prijavljenKorisnik.TipKorisnika.ToString() == "pacijent")
                {
                    btnZakaziTermin.Visibility = Visibility.Visible;
                    FrmPacijent frm = new FrmPacijent(prijavljenKorisnik, prijavljenKorisnik);
                    frm.ShowDialog();

                }
                else if (prijavljenKorisnik.TipKorisnika.ToString() == "administrator")
                {
                    FrmAdmin frm = new FrmAdmin(prijavljenKorisnik);
                    frm.ShowDialog();

                }
            }
        }

        private void cbTipKorisnika_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            osveziPrikazKorisnici();
        }

        private void btnRegistracijaPacijenta_Click(object sender, RoutedEventArgs e)
        {
            FrmDodajIzmeni_Korisnik frm = new FrmDodajIzmeni_Korisnik(null, true, null);
            frm.ShowDialog();
            if (frm.DialogResult == true)//ako je kliknuto potvrdno na toj formi
                osveziPrikazKorisnici();
        }

        void osveziPrikazKorisnici() {

            if (cbTipKorisnika.SelectedIndex == 0)
                dg1.ItemsSource = DbManagement.lstKorisnici.Where(x => x.getObrisan() == false && (x.TipKorisnika == TipKorisnika.lekar || x.TipKorisnika == TipKorisnika.pacijent));
            if (cbTipKorisnika.SelectedIndex == 1)
                dg1.ItemsSource = DbManagement.lstKorisnici.Where(x => x.getObrisan() == false && x.TipKorisnika == TipKorisnika.lekar);
            if (cbTipKorisnika.SelectedIndex == 2)
                dg1.ItemsSource = DbManagement.lstKorisnici.Where(x => x.getObrisan() == false && x.TipKorisnika == TipKorisnika.pacijent);
        }

        private void btnZakaziTermin_Click(object sender, RoutedEventArgs e)
        {
            if (dg1.SelectedIndex > -1)
            {
                if (rbKorisnici.IsChecked == true)
                {
                    Korisnik k = (Korisnik)dg1.SelectedItem;
                    if (k.TipKorisnika == TipKorisnika.lekar)
                    {
                        if (prijavljenKorisnik.TipKorisnika == TipKorisnika.pacijent)
                        {
                            FrmDodavanjeTermina frm = new FrmDodavanjeTermina(k, prijavljenKorisnik);
                            frm.ShowDialog();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Morate selektovati lekara!");
                    }
                }
                else
                {
                    MessageBox.Show("Morate selektovati lekara!");
                }
            }
            else
            {
                MessageBox.Show("Morate selektovati lekara!");
            }
        }

        private void tbPretragaPodataka_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (rbDomoviZdravlja.IsChecked==true)
            {
                dg1.ItemsSource = DbManagement.lstDomoviZdravlja.Where(
                    dz => 
                    dz.getObrisan()==false &&(
                    dz.Naziv.ToLower().Contains(tbPretragaPodataka.Text.ToLower()) || 
                    dz.getAdresa().Ulica.ToLower().Contains(tbPretragaPodataka.Text.ToLower()) || 
                    dz.getAdresa().Broj.ToString().Contains(tbPretragaPodataka.Text)
                    )
                    );
            }
            else if (rbKorisnici.IsChecked == true)
            {
                dg1.ItemsSource = DbManagement.lstKorisnici.Where(
                       k=>
                       k.getObrisan()==false &&(
                       k.ImePrezime.ToLower().Contains(tbPretragaPodataka.Text.ToLower()) ||
                       k.Email.ToLower().Contains(tbPretragaPodataka.Text.ToLower()) ||
                       k.getAdresa().Ulica.ToLower().Contains(tbPretragaPodataka.Text.ToLower()) ||
                       k.getAdresa().Broj.ToString().Contains(tbPretragaPodataka.Text)
                       )
                    );
            }
            else if (rbTerapije.IsChecked == true)
            {
                dg1.ItemsSource = DbManagement.lstTerminiTerapije.Where(
                       t => 
                       t.getObrisan()==false && (
                       t.OpisTerapije.ToLower().Contains(tbPretragaPodataka.Text.ToLower()) ||
                       t.getLekar().ImePrezime.ToLower().Contains(tbPretragaPodataka.Text.ToLower()) ||
                        t.getLekar().Jmbg.ToLower().Contains(tbPretragaPodataka.Text.ToLower())
                        )
                    );
            }
        }
    }
}
