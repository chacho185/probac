﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomoviZdravlja.Model
{
    public class Terapija:Termin
    {
        string opisTerapije;

        public Terapija(int id, Korisnik lekar, Korisnik pacijent, DateTime datum, bool statusDostupnost, bool obrisan, string opisTerapije)
            :base(id, lekar, pacijent, datum, statusDostupnost, obrisan)
        {
            this.OpisTerapije = opisTerapije;
        }

        public string OpisTerapije { get => opisTerapije; set => opisTerapije = value; }
    }
}
