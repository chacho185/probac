﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomoviZdravlja.Model
{
    public class DomZdravlja
    {
        int id;
        string naziv;
        Adresa adresaObj;
        bool obrisan = false;
        string adresa;

        public DomZdravlja(int id, string naziv, Adresa adresaObj, bool obrisan)
        {
            this.Id = id;
            this.Naziv = naziv;
            this.adresaObj = adresaObj;
            this.obrisan = obrisan;
            this.Adresa = adresaObj.toString();
        }

        public void obrisi()
        {
            this.obrisan = true;
        }

        public int Id { get => id; set => id = value; }
        public string Naziv { get => naziv; set => naziv = value; }
        public string Adresa { get => adresa; set => adresa = value; }



        //public Adresa Adresa { get => adresa; set => adresa = value; }
        public void setAdresa(Adresa adresa)
        {
            this.adresaObj = adresa;
        }
        public Adresa getAdresa()
        {
            return adresaObj;
        }

        public bool getObrisan()
        {
            return obrisan;
        }
    }
}
