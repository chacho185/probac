﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomoviZdravlja.Model
{
    public class Termin
    {
        int id;
        Korisnik lekarObj;
        Korisnik pacijentObj;
        DateTime datum;
        bool statusDostupnost;
        bool obrisan;

        string lekar;
        string pacijent; //U datagrid preko Items Source prikazuje po atributima..

        public Termin(int id, Korisnik lekarObj, Korisnik pacijentObj, DateTime datum, bool statusDostupnost, bool obrisan)
        {
            this.Id = id;
            this.lekarObj = lekarObj;
            this.pacijentObj = pacijentObj;
            this.Datum = datum;
            this.statusDostupnost = statusDostupnost;
            this.obrisan = obrisan;

            
        }

        public void obrisi()
        {
            this.obrisan = true;
        }


        public int Id { get => id; set => id = value; }



        //public Korisnik Lekar { get => lekarObj; set => lekarObj = value; }
        public void setLekar(Korisnik lekar)
        {
            this.lekarObj = lekar;
        }
        public Korisnik getLekar()
        {
            return lekarObj;
        }
        public string Lekar { get => lekar; set => lekar = value; }



        //public Korisnik Pacijent { get => pacijentObj; set => pacijentObj = value; }
        public void setPacijent(Korisnik pacijent)
        {
            this.pacijentObj = pacijent;
        }
        public Korisnik getPacijent()
        {
            return pacijentObj;
        }
        public string Pacijent { get => pacijent; set => pacijent = value; }



        public DateTime Datum { get => datum; set => datum = value; }
        //public bool StatusDostupnost { get => statusDostupnost; set => statusDostupnost = value; }
        public void setStatusDostupnost(bool status)
        {
            this.statusDostupnost = status;
        }
        public bool getStatusDostupnost()
        {
            return this.statusDostupnost;
        }


        public bool getObrisan()
        {
            return obrisan;
        }

    }
}
