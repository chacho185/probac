﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DomoviZdravlja.Model
{
    public class DbManagement
    {
        public static ObservableCollection<Korisnik> lstKorisnici = new ObservableCollection<Korisnik>();
        public static ObservableCollection<DomZdravlja> lstDomoviZdravlja = new ObservableCollection<DomZdravlja>();
        public static ObservableCollection<Terapija> lstTerminiTerapije = new ObservableCollection<Terapija>();

        public static string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\ACERe1\Downloads\DomoviZdravlja\DomoviZdravlja\DomoviZdravlja.mdf;Integrated Security=True";
        //public static string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=..\..\..\DomoviZdravlja.mdf;Integrated Security=True";


        public static void ucitajListe()
        {
            lstKorisnici = ucitajKorisnike();
            lstDomoviZdravlja = ucitajDomoveZdravlja();
            lstTerminiTerapije = ucitajTermine_Terapije();
        }


        public static void insert_update_delete(string sqlModify)
        {

            SqlConnection conn = new SqlConnection(connString);

            conn.Open();
            SqlCommand cmd = new SqlCommand(sqlModify, conn);
            try
            {
                // MessageBox.Show(sqlModify);
                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            finally
            {
                conn.Close();
            }


        }

        public static ObservableCollection<Adresa> ucitajAdrese() {

            ObservableCollection<Adresa> lstAdrese = new ObservableCollection<Adresa>();

            SqlConnection conn = new SqlConnection(connString);
            conn.Open();

            SqlCommand sqlCmd = new SqlCommand("select * from Adresa", conn);
            SqlDataReader sqlDataRdr = sqlCmd.ExecuteReader();
            while (sqlDataRdr.Read())
            {
                int id = Convert.ToInt32(sqlDataRdr.GetValue(0));
                string ulica = sqlDataRdr.GetValue(1).ToString();
                int broj = Convert.ToInt32(sqlDataRdr.GetValue(2));
                string grad = sqlDataRdr.GetValue(3).ToString();
                string drzava = sqlDataRdr.GetValue(4).ToString();
                bool obrisan = Convert.ToBoolean(sqlDataRdr.GetValue(5));

                Adresa adresa = new Adresa(id, ulica, broj, grad, drzava, obrisan);
                lstAdrese.Add(adresa);
            }

            sqlDataRdr.Close();
            sqlCmd.Dispose();
            conn.Close();

            return lstAdrese;


        }

        public static ObservableCollection<DomZdravlja> ucitajDomoveZdravlja()
        {

            ObservableCollection<DomZdravlja> lstDomoviZdravlja = new ObservableCollection<DomZdravlja>();

            SqlConnection conn = new SqlConnection(connString);
            conn.Open();

            SqlCommand sqlCmd = new SqlCommand("select * from DomZdravlja", conn);
            SqlDataReader sqlDataRdr = sqlCmd.ExecuteReader();
            while (sqlDataRdr.Read())
            {

                int id = Convert.ToInt32(sqlDataRdr.GetValue(0));
                string naziv = sqlDataRdr.GetValue(1).ToString();

                int idAdresa = Convert.ToInt32(sqlDataRdr.GetValue(2));
                Adresa adresa = ucitajAdrese().Where(a => a.Id == idAdresa).FirstOrDefault();

                bool obrisan = Convert.ToBoolean(sqlDataRdr.GetValue(3));

                DomZdravlja domZdr = new DomZdravlja(id, naziv, adresa, obrisan);
                lstDomoviZdravlja.Add(domZdr);
            }

            sqlDataRdr.Close();
            sqlCmd.Dispose();
            conn.Close();

            return lstDomoviZdravlja;


        }

        public static ObservableCollection<Korisnik> ucitajKorisnike()
        {

            ObservableCollection<Korisnik> lstKorisnici = new ObservableCollection<Korisnik>();

            SqlConnection conn = new SqlConnection(connString);
            conn.Open();

            SqlCommand sqlCmd = new SqlCommand("select * from Korisnik", conn);
            SqlDataReader sqlDataRdr = sqlCmd.ExecuteReader();
            while (sqlDataRdr.Read())
            {

                string jmbg = sqlDataRdr.GetValue(0).ToString();
                string lozinka = sqlDataRdr.GetValue(1).ToString();
                string imePrezime = sqlDataRdr.GetValue(2).ToString();
                string email = sqlDataRdr.GetValue(3).ToString();

                int idAdresa = Convert.ToInt32(sqlDataRdr.GetValue(4));
                Adresa adresa = ucitajAdrese().Where(a => a.Id == idAdresa).FirstOrDefault();

                string pol = sqlDataRdr.GetValue(5).ToString();

                string tipKorisnikaString= sqlDataRdr.GetValue(6).ToString();
                TipKorisnika tipKorisnika = (TipKorisnika)Enum.Parse(typeof(TipKorisnika), tipKorisnikaString);

                int? idDomZdravlja = -1;
                DomZdravlja domZdravlja = null;
                if (!(sqlDataRdr.GetValue(7) is DBNull))
                {
                    idDomZdravlja = Convert.ToInt32(sqlDataRdr.GetValue(7));
                    domZdravlja = ucitajDomoveZdravlja().Where(dz => dz.Id == idDomZdravlja).FirstOrDefault();
                }

                bool obrisan = Convert.ToBoolean(sqlDataRdr.GetValue(8));


                Korisnik korisnik = new Korisnik(jmbg, lozinka, imePrezime, email, adresa, pol, tipKorisnika, domZdravlja, obrisan);
                lstKorisnici.Add(korisnik);
            }

            sqlDataRdr.Close();
            sqlCmd.Dispose();
            conn.Close();

            return lstKorisnici;


        }

        public static ObservableCollection<Terapija> ucitajTermine_Terapije() {

            ObservableCollection<Terapija> lstTerminiTerapije = new ObservableCollection<Terapija>();
            SqlConnection conn = new SqlConnection(connString);
            conn.Open();

            SqlCommand sqlCmd = new SqlCommand("select * from Termin", conn);
            SqlDataReader sqlDataRdr = sqlCmd.ExecuteReader();
            while (sqlDataRdr.Read())
            {

                int id = Convert.ToInt32(sqlDataRdr.GetValue(0));

                string jmbgLekar = sqlDataRdr.GetValue(1).ToString();
                Korisnik lekar = ucitajKorisnike().Where(k => k.Jmbg == jmbgLekar).FirstOrDefault();

                string jmbgPacijent = sqlDataRdr.GetValue(2).ToString();
                Korisnik pacijent = ucitajKorisnike().Where(k => k.Jmbg == jmbgPacijent).FirstOrDefault();

            
                DateTime datum = Convert.ToDateTime(sqlDataRdr.GetValue(3).ToString());
                bool statusDostupnost = Convert.ToBoolean(sqlDataRdr.GetValue(4));
                string opisTerapije = sqlDataRdr.GetValue(5).ToString();
                bool obrisan = Convert.ToBoolean(sqlDataRdr.GetValue(6));

                //if (opisTerapije.Equals(""))
                //{
                //    Termin termin = new Termin(id, lekar, pacijent, datum, statusDostupnost, obrisan);
                //    lstTerminiTerapije.Add(termin);
                //}
                //else
                //{
                Terapija terapija = new Terapija(id, lekar, pacijent, datum, statusDostupnost, obrisan, opisTerapije);

                if(terapija.getLekar()!=null)
                    terapija.Lekar =terapija.getLekar().Jmbg + ", Ime i prezime: " + terapija.getLekar().ImePrezime;
                if(terapija.getPacijent()!=null)
                    terapija.Pacijent =terapija.getPacijent().Jmbg + ", Ime i prezime: " + terapija.getPacijent().ImePrezime;
                lstTerminiTerapije.Add(terapija);
                //}

            }

            sqlDataRdr.Close();
            sqlCmd.Dispose();
            conn.Close();

            return lstTerminiTerapije;
        }



    }
}
