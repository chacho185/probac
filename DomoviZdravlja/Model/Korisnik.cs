﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomoviZdravlja.Model
{
    public enum TipKorisnika { lekar, pacijent, administrator};
    public class Korisnik
    {
        string jmbg;
        string lozinka;
        string imePrezime;
        string email;
        Adresa adresaObj;
        string pol;
        TipKorisnika tipKorisnika;
        DomZdravlja domZdravlja;
        bool obrisan = false;

        string adresa;

        public List<Termin> lstTermini = new List<Termin>();
        public List<Terapija> lstTerapije = new List<Terapija>();

        public Korisnik(string jmbg, string lozinka, string imePrezime, string email, Adresa adresaObj, string pol, TipKorisnika tipKorisnika, DomZdravlja domZdravlja, bool obrisan)
        {
            this.Jmbg = jmbg;
            this.lozinka = lozinka;
            this.ImePrezime = imePrezime;
            this.Email = email;
            this.adresaObj = adresaObj;
            this.Pol = pol;
            this.TipKorisnika = tipKorisnika;
            this.domZdravlja = domZdravlja;
            this.obrisan = obrisan;

            this.Adresa = adresaObj.toString();
        }
        public Korisnik(Korisnik k)
        {
            this.Jmbg = k.Jmbg;
            this.lozinka = k.lozinka;
            this.ImePrezime = k.ImePrezime;
            this.Email = k.Email;
            this.adresaObj = k.adresaObj;
            this.Pol = k.Pol;
            this.TipKorisnika = k.TipKorisnika;
            this.domZdravlja = k.domZdravlja;
            this.obrisan = k.obrisan;

            this.Adresa = k.adresaObj.toString();
        }

        public void obrisi()
        {
            this.obrisan = true;
        }


        public string Jmbg { get => jmbg; set => jmbg = value; }


        //public string Lozinka { get => lozinka; set => lozinka = value; }
        public void setLozinka(string lozinka)
        {
            this.lozinka = lozinka;
        }
        public string getLozinka()
        {
            return lozinka;
        }



        public string ImePrezime { get => imePrezime; set => imePrezime = value; }
        public string Email { get => email; set => email = value; }


        public string Adresa { get => adresa; set => adresa = value; }
        //public Adresa Adresa { get => adresaObj; set => adresaObj = value; }
        public void setAdresa(Adresa adresa)
        {
            this.adresaObj = adresa;
        }
        public Adresa getAdresa()
        {
            return adresaObj;
        }


        public string Pol { get => pol; set => pol = value; }
        public TipKorisnika TipKorisnika { get => tipKorisnika; set => tipKorisnika = value; }
        //public DomZdravlja DomZdravlja { get => domZdravlja; set => domZdravlja = value; }
        public void setDomZdravlja(DomZdravlja dz)
        {
            this.domZdravlja = dz;
        }
        public DomZdravlja getDomZdravlja()
        {
            return this.domZdravlja;
        }

        public bool getObrisan()
        {
            return obrisan;
        }


        public string toString()
        {
            string prikaz = imePrezime + " " + email;
            if (domZdravlja != null)
                prikaz += " Dom zdravlja: " + domZdravlja.Naziv;
            return prikaz;
        }
        
    }
}
