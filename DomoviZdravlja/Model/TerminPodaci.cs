﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomoviZdravlja.Model
{
    public class TerminPodaci
    {
        string termin;

        public TerminPodaci(string termin)
        {
            this.Termin = termin;
        }

        public string Termin { get => termin; set => termin = value; }
    }
}
