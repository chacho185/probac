﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomoviZdravlja.Model
{
    public class Adresa
    {
        int id;
        string ulica;
        int broj;
        string grad;
        string drzava;
        bool obrisan = false;

        public Adresa(int id, string ulica, int broj, string grad, string drzava, bool obrisan)
        {
            this.Id = id;
            this.Ulica = ulica;
            this.Broj = broj;
            this.Grad = grad;
            this.Drzava = drzava;
            this.obrisan = obrisan;
        }

        public Adresa(Adresa adresa)
        {
            this.Id = adresa.Id;
            this.Ulica = adresa.Ulica;
            this.Broj = adresa.Broj;
            this.Grad = adresa.Grad;
            this.Drzava = adresa.Drzava;
            this.obrisan = adresa.obrisan;
        }

        public void obrisi() {
            this.obrisan = true;
        }

        public int Id { get => id; set => id = value; }
        public string Ulica { get => ulica; set => ulica = value; }
        public int Broj { get => broj; set => broj = value; }
        public string Grad { get => grad; set => grad = value; }
        public string Drzava { get => drzava; set => drzava = value; }

        public bool getObrisan()
        {
            return obrisan;
        }

        public string toString() {
            return ulica + " " + broj + " " + grad + " " + drzava;
         }
    }
}
